import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import DogList from '@/components/DogList.vue'
import SingleDog from '@/components/SingleDog.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      component: HomeView
    },
    {
      path: '/dogs',
      component:DogList
    },
    {
      path: '/dogs/:id',
      component:SingleDog
    }
  ]
})

export default router
