
export interface Dog {
    id?: number;
    name: string;
    breed: string;
    birthdate: string;
    image: string;
    prix: number;
}
